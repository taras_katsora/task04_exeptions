package yard.tree;

public class AppleTree extends TreeManeger {
    private int appleNumber;

    public AppleTree(String model, int highTree, int widthTree,int appleNumber){
        super(model,highTree, widthTree);
       this.appleNumber = appleNumber;
    }

    public int getAppleNumber() {
        return appleNumber;
    }

    public void setAppleNumber(int appleNumber) {
        this.appleNumber = appleNumber;
    }

    @Override
    public String toString() {
        return "AppleTree{" +
                "appleNumber=" + appleNumber +
                "} " + super.toString();
    }
}
