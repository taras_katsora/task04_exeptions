package yard.tree;

public class TreeManeger implements Comparable<TreeManeger>{
    private String model;
    private int highTree;
    private int widthTree;


    public TreeManeger(String model, int highTree, int widthTree) {
        this.model = model;
        this.highTree = highTree;
        this.widthTree = widthTree;
    }



    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getHighTree() {
        return highTree;
    }

    public void setHighTree(int highTree) {
        this.highTree = highTree;
    }

    public int getWidthTree() {
        return widthTree;
    }

    public void setWidthTree(int widthTree) {
        this.widthTree = widthTree;
    }

    @Override
    public int compareTo(TreeManeger o) {
        return 0;
    }
}
