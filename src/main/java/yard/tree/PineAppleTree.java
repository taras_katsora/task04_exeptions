package yard.tree;

public class PineAppleTree extends TreeManeger {
    private int pineAppleNumber;

    public PineAppleTree(String model, int highTree, int widthTree, int pineAppleNumber) {
        super(model, highTree, widthTree);
        this.pineAppleNumber = pineAppleNumber;
    }

    public int getPineAppleNumber() {
        return pineAppleNumber;
    }

    public void setPineAppleNumber(int pineAppleNumber) {
        this.pineAppleNumber = pineAppleNumber;
    }

    @Override
    public String toString() {
        return "PineAppleTree{" +
                "pineAppleNumber=" + pineAppleNumber +
                "} " + super.toString();
    }
}
