package com.epam;

import yard.tree.TreeManeger;
import yard.tree.AppleTree;
import yard.tree.PineAppleTree;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class TreeOrganisator {
    private Random random = new Random(System.currentTimeMillis());
    private List<String> models = Arrays.asList("treeOne","treeTwo","treeThree");
    private List<TreeManeger> trees = new ArrayList<>();

    public void addTreeManager(TreeManeger treeManeger) {
        trees.add(treeManeger);
    }

    public List<TreeManeger> getTrees() {
        return trees;
    }

    public void generateTrees(int number) {
        trees.clear();

            for (int i = 0; i < number; i++) {
                int type = random.nextInt(4);
                switch (type) {
                    case 0: {
                        trees.add(generatePineAppleTree());
                        break;
                    }
                    case 1: {
                        trees.add(generateAppleTree());
                        break;
                    }
                    case 2: {

                        break;
                    }
                    case 3: {

                        break;
                    }
                    default:
                        throw new IllegalArgumentException();
                }
            }
    }

    private TreeManeger generatePineAppleTree() {
        String model = getModel();
        return new PineAppleTree(model,random.nextInt(60), random.nextInt(20), random.nextInt(1000));
    }

    private TreeManeger generateAppleTree() {
        String model = getModel();
        return new AppleTree(model, random.nextInt(60), random.nextInt(20),random.nextInt(1000));
    }

    private String getModel() {
        return models.get(random.nextInt(models.size()));
    }

}
