package mvc;

import yard.tree.TreeManeger;

public class View {

    public void displayTreeManager(TreeManeger treeManeger) {
        System.out.println(treeManeger.toString());
    }

    public void displayText(String mainText) {
        System.out.println(mainText);
    }
}
