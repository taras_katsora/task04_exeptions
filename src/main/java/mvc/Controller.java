package mvc;

import yard.tree.TreeManeger;
import com.epam.TreeOrganisator;

import java.util.*;

public class Controller {
    private String mainText =
            "Please select option:\n"
                    +
                    "0) quit\n"
                    +
                    "1) generate trees\n"
                    +
                    "2) show generated trees\n"
                    +
                    "3) show total hight\n";


    private View mainView;
    private static TreeOrganisator model;

    private Scanner scan;
    private int number;

    public Controller() {
        mainView = new View();
        scan = new Scanner(System.in);
        model = new TreeOrganisator();
    }

    public void start()throws InputMismatchException {
        mainView.displayText(mainText);
        System.out.println("Enter number of tree: ");
            int number = scan.nextInt();
        System.out.println("Now chose one of the meny points: ");
            String c = scan.next("[0-4]");
        while (!c.equals("0")) {
            switch (c) {
                case "0": {
                    return;
                }
                case "1": {
                    generateTrees(number);
                    break;
                }
                case "2": {
                    for (TreeManeger treeManeger : getTreesSorted()) {
                        mainView.displayTreeManager(treeManeger);
                    }
                    break;
                }
                case "3": {
                    mainView.displayText("total high of trees: " + getTreeHigh());

                    break;
                }


                default: {
                    System.out.println("Please insert numbers in range[0-4]!");
                    break;
                }
            }
            mainView.displayText(mainText);
            c = scan.next("[0-3]");
        }
    }

    public void generateTrees(int number)  {
        model.generateTrees(number);
    }

    public int getTreeHigh()throws NullPointerException {
        int total = 0;
        for (TreeManeger treeManeger : model.getTrees()) {
            try {
                total += treeManeger.getHighTree();
            }catch (NullPointerException e){
                getTreeHigh();
            }
        }
        return total;
    }

    public List<TreeManeger> getTreesSorted() {
        List<TreeManeger> sorted = new ArrayList<>(model.getTrees());
        Collections.sort(sorted);
        return sorted;
    }
}
